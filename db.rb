require 'data_mapper'

DataMapper.setup :default, "sqlite3://#{Dir.pwd}/project.db"

class Template
  include DataMapper::Resource

  property :id,         Serial
  property :name,      String
  property :created_at, DateTime
end

class Current
  include DataMapper::Resource

  property :id,         Serial
  property :project,    String
  property :tmpl,       String
  property :created_at, DateTime
  property :updated_at, DateTime
end
DataMapper.finalize
# DataMapper.auto_migrate!
DataMapper.auto_upgrade!
