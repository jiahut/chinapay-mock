require 'thrift'
$:.push('gen-rb')


require 'cipher'

transport = Thrift::BufferedTransport.new(Thrift::Socket.new('192.168.88.159', 9090))
# transport = Thrift::BufferedTransport.new(Thrift::Socket.new('192.168.88.103', 9090))
protocol = Thrift::BinaryProtocol.new(transport)
proto_1 = Thrift::MultiplexedProtocol.new(protocol, "cipher")
cipher = Cipher::Client.new(proto_1)
transport.open()

puts cipher.encode("lo")

text = File.open("test_cipher/encode.txt","r").read
puts text
# text = <<-XML
puts cipher.encode(text)

text = File.open("test_cipher/decode.txt","r").read
puts text
puts cipher.decode(text)
