namespace java com.upsmart.thrift.gen.tt

service Cipher {
	string encode(1: string text)
	string decode(1: string text)
}
