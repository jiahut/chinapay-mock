namespace java com.upsmart.thrift.gen.chinapay

# struct Request {
#   1: string merId,
#   2: string merDate,
#   3: string merSeqId,
#   4: string cardNo,
#   5: string usrName,
#   6: string openBank,
#   7: string prov,
#   8: string city,
#   9: string trajsAkt,
#   10: string purpose,
#   11: string subBank,
#   12: boolean flag,
#   13: string  version
# }

struct Respond {
    1: string responseCode
    2: string merId
    3: string merDate
    4: string merSeqId
    5: string cpDate
    6: string cpSeqId
    7: string transAmt
    8: string stat
    9: string cardNo
}


service Signature {
  string sign(1: Respond res)
  bool  verify()
}
