namespace java com.upsmart.thrift.gen.bypay

service DesCodec {
	string encrypt(1: string text)
	string decrypt(1: string text)
}
