require 'thrift'
$:.push('gen-rb')


require 'signature'
require 'sign_constants'

transport = Thrift::BufferedTransport.new(Thrift::Socket.new('localhost', 9090))
# transport = Thrift::BufferedTransport.new(Thrift::Socket.new('192.168.88.103', 9090))
protocol = Thrift::BinaryProtocol.new(transport)
proto_sign  = Thrift::MultiplexedProtocol.new(protocol, "sign")
sign = Signature::Client.new(proto_sign)
transport.open()

res = Respond.new
res.merId = "808080211388370"
res.stat = "3"

puts sign.sign(res)
