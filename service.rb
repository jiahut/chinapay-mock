require 'sinatra'
require './db'
require 'nokogiri'
# require 'debugger'
# require './signature'

if settings.development?
  require 'sinatra/reloader'
end


require 'thrift'
$:.push('thrift/gen-rb')


require 'date'
require 'signature'
require 'sign_constants'
require 'cipher'
require 'des_codec'

transport = Thrift::BufferedTransport.new(Thrift::Socket.new('192.168.88.159', 9090))
# transport = Thrift::BufferedTransport.new(Thrift::Socket.new('192.168.88.103', 9090))
protocol = Thrift::BinaryProtocol.new(transport)
proto_sign  = Thrift::MultiplexedProtocol.new(protocol, "sign")
sign = Signature::Client.new(proto_sign)

proto_1 = Thrift::MultiplexedProtocol.new(protocol, "cipher")
cipher = Cipher::Client.new(proto_1)


proto_2 = Thrift::MultiplexedProtocol.new(protocol, "des_codec")
des_codec = DesCodec::Client.new(proto_2)

transport.open()
helpers do

  def date(format = '%Y-%m-%d')
    return DateTime.now.strftime(format)
  end

  def rand_str(size = 8)
    o = [('0'..'9'), ('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
    (0...size).map { o[rand(o.length)] }.join
  end

  def rand_num(size = 4)
    o = ('0'..'9').to_a
    (0...size).map { o[rand(o.length)]}.join
  end
end

get "/bypayauth" do
  _tmpl = Current.first(project: "bypayauth").tmpl
  _the_tmpl = File.join("bypayauth", _tmpl)
  # tmpl_res = File.read(_the_tmpl)
  tmpl_res = erb _the_tmpl.to_sym
  status,code,origin_res = tmpl_res.split("|",3)
  puts "___________after______________"
  puts "status = " + status
  puts "code   = " + code
  puts "origin = " + origin_res
  res = des_codec.encrypt(origin_res)
  puts res
  if status.to_i == 0
     [status,code,res].join("|")
  else
     [status,res, "xxx"].join("|")
  end
end

post "/bypayauth" do
  _tmpl = Current.first(project: "bypayauth").tmpl
  _the_tmpl = File.join("bypayauth", _tmpl)
  # tmpl_res = File.read(_the_tmpl)
  tmpl_res = erb _the_tmpl.to_sym
  status,code,origin_res = tmpl_res.split("|",3)
  puts "___________after______________"
  puts "status = " + status
  puts "code   = " + code
  res = des_codec.encrypt(origin_res)
  puts res
  if status.to_i == 0
     [status,code,res].join("|")
  else
     [status,res, "xxx"].join("|")
  end
end

get "/TTInterface/services/HyrtInterface" do
   body_str = request.body.read
   doc = Nokogiri::XML <<-XML
	#{body_str.split("\r\n")[1]}
   XML
   puts doc
   puts  doc.xpath("//soapenv:Body").children.first.name
   # puts env
   # puts request.body.read
   File.open("views/test.xml","r").readlines
end

post "/TTInterface/services/HyrtInterface" do
   tmpl = Current.first(project: "TTInterface").tmpl
   body_str = request.body.read
   doc = Nokogiri::XML <<-XML
	#{body_str.split("\r\n")[1]}
   XML
   action = doc.xpath("//soapenv:Body").children.first.name
   puts action

   origin_res = File.open("views/TTInterface/#{action}_#{tmpl}.erb","r").read
   puts origin_res
   puts "___________after______________"
   origin_res =  origin_res.gsub("\n","").gsub("\t","")
   puts origin_res
   res = cipher.encode(origin_res)

   ret = erb "TTInterface/_respond".to_sym ,locals: { prefix: action,  res: res }
   puts ret
   # File.open("views/test.1.xml","w") do |f|
    #   f.write(ret)
   # end
   ret
   # puts env
   # puts request.body.read
   # File.open("views/test.xml","r").read
   #File.open("views/test.1.xml","r").readlines
end

post "/b2blib/:tmpl" do
   File.open("views/b2blib/" + params[:tmpl],"r").readlines
end

get "/b2blib/:tmpl" do
   File.open("views/b2blib/" + params[:tmpl],"r").readlines
end

get '/:name' do

  _vals =  params.merge(binding.send(:local_variables).reduce({}) do |acc, v|
                          acc[v] = binding.eval(v.to_s) unless v == :_
                          acc
    end)
  _tmpl = Current.first(project: "chinapay").tmpl
  _the_tmpl = File.join(params[:name], _tmpl)
  _ret = erb _the_tmpl.to_sym ,locals: _vals
  puts _ret

  resp = Respond.new
  _ret.split("&").each do |field|
    name, value = field.split("=")
    eval("resp.#{name}='#{value}'")
  end
  # resp.merId = "808080211388370"

  _sign = sign.sign(resp)
  puts _sign
  #_pri_key = OpenSSL::PKey::RSA.new(File.read("/Users/jazz/.ssh/id_rsa"))
  #_sign = Signature.sign(_ret.split("&").sort.join("&"),_pri_key)
  _ret.strip + "&chkvalue=" + _sign
end


# get '/' do

#   #puts params
#   # hello = params[:hello]

#   # my = params[:hello]
#   # local_names = binding.send(:local_variables)
#   # locals = local_names.reduce({}) do |acc, v|
#   #   acc[v] = binding.eval(v.to_s) unless v == :_
#   #   acc
#   # end
#   # puts my
#   # puts locals
#   # puts locals.merge(params)
#   # locals = locals.merge(params)
#   _vals =  params.merge(binding.send(:local_variables).reduce({}) do |acc, v|
#                           acc[v] = binding.eval(v.to_s) unless v == :_
#                           acc
#     end)
#   _tmpl = Current.first.tmpl

#   erb _tmpl.to_sym ,locals: _vals
# end

get '/template/change/:project/:template' do
  # tmpl = params[:template]
  # before = File.read(".template").strip
  # if tmpl
  #   File.open(".template","w") do |file|
  #     file.write tmpl
  #   end
  #   "the template had changed from #{before} to #{tmpl}"
  # end
  project = params[:project]
  r = Current.first_or_create(project: project)
  before = r.tmpl
  r.tmpl = params[:template].strip
  r.save!
  "the project #{project} had changed from #{before} to #{r.tmpl}\n"
end

get '/template/list' do
  retval = "the template are\n"
  Current.all.each do |current|
    retval += "\t#{current.project} -> #{current.tmpl}\n"
  end
  retval
  # File.read(".template").strip
end

get "/template/delete/:project" do
  project = params[:project]
  Current.first(project: project).destroy
  "the project #{project} had deleted\n"
end
